


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

private static final Logger logger= LogManager.getRootLogger();

    public static int menu1() throws ExceptionConnect {
        Scanner sc = new Scanner(System.in);
        System.out.println("------Menu1-------");
        System.out.println("0.Sortir");
        System.out.println("1.Motos");
        System.out.println("2.Concesionaris");
        int menu = sc.nextInt();
        try {
            if (menu == 1) {
                menu2();
            } else if (menu == 2) {
                menu3();
            } else if (menu == 0) {
                System.out.println("Programa finalitzat");
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
        return menu;

    }

    public static void menu2() throws ExceptionConnect {
        System.out.println("------Menu2-------");
        System.out.println("0.Sortir");
        System.out.println("1.Crear la taula moto");
        System.out.println("2.Borrar la taula moto");
        System.out.println("3.Añadir una moto");
        System.out.println("4.Llistar una moto per matricula");
        System.out.println("5.Llistar totes les motos");
        System.out.println("6.Modificar una moto");
        System.out.println("7.Borrar una moto");
        System.out.println("8.Pasar totes les motos a una llista");

        DAOMotos daoMotos = new DAOJDBCMotos();
        Scanner sc = new Scanner(System.in);
        daoMotos.getConnection("jdbc:mysql://localhost:3306/basededatos", "root", "jupiter");
        int a = 1;
        try {

            while (a != 0) {
                int opcio = sc.nextInt();
                Scanner scmotos = new Scanner(System.in);

                switch (opcio) {

                    case 0:
                        a = 0;
                        break;
                    case 1:
                        daoMotos.crearTaula();
                        Thread.sleep(5000);

                        break;
                    case 2:
                        daoMotos.borrarTaula();
                        Thread.sleep(5000);

                        break;
                    case 3:
                        System.out.println("Dades de la moto a inserir:");
                        System.out.println("Matricula de la moto");
                        String matricula = scmotos.next();
                        System.out.println("Model de la moto");
                        String nom = scmotos.next();
                        System.out.println("Data de matriculacio format(YYYY-MM-DD)");
                        String data_matri = scmotos.next();
                        Motos motos = new Motos(nom, matricula, data_matri);
                        daoMotos.inserir(motos);
                        Thread.sleep(5000);

                        break;
                    case 4:
                        System.out.println("Matricula de la moto");
                        matricula = scmotos.next();
                        daoMotos.recuperarPerId(matricula);
                        Thread.sleep(5000);

                        break;
                    case 5:
                        daoMotos.recuperarTots();
                        Thread.sleep(5000);

                        break;
                    case 6:
                        System.out.println("Dades de la moto a modificar:");
                        System.out.println("Matricula de la moto");
                        matricula = scmotos.next();
                        System.out.println("Model de la moto");
                        nom = scmotos.next();
                        System.out.println("Data de matriculacio format(YYYY-MM-DD)");
                        data_matri = scmotos.next();
                        motos = new Motos(nom, matricula, data_matri);

                        System.out.println("Nova matricula");
                        String matr = scmotos.next();
                        System.out.println("Nou model");
                        String modelnou = scmotos.next();
                        System.out.println("Nova data");
                        String datanova = scmotos.next();

                        String[] params = new String[]{modelnou, matr, datanova};


                        daoMotos.update(motos, params);
                        Thread.sleep(5000);

                        break;
                    case 7:
                        System.out.println("Dades de la moto a inserir:");
                        System.out.println("Matricula de la moto");
                        matricula = scmotos.next();


                        daoMotos.delete(matricula);
                        Thread.sleep(5000);

                        break;
                    case 8:
                        daoMotos.getAll();
                        Thread.sleep(5000);

                        break;
                    default:
                        System.out.println("opcio no valida tornara al menu1");
                        a = 0;

                }
                if (a == 0) {
                    menu1();
                }
                {
                    menu2();
                }

                daoMotos.closeConnection();
            }
        } catch (Exception e) {
            a = 0;
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }


    public static void menu3() throws ExceptionConnect {
        System.out.println("------Menu3-------");
        System.out.println("0.Sortir");
        System.out.println("1.Crear la taula concesionari");
        System.out.println("2.Borrar la taula concesionari");
        System.out.println("3.Añadir un concesionari");
        System.out.println("4.Llistar un concesionari per dni");
        System.out.println("5.Llistar tots els concesionari");
        System.out.println("6.Modificar un concesionari");
        System.out.println("7.Borrar una concesionari");
        System.out.println("8.Pasar tots els concesionari a una llista");

        Scanner sc = new Scanner(System.in);
        int a = 1;
        DAOConcesionari daoConcesionari = new DAOJDBCConcesionari();
        daoConcesionari.getConnection("jdbc:mysql://localhost:3306/basededatos", "root", "jupiter");


        try {
            while (a != 0) {
                int opcio = sc.nextInt();
                switch (opcio) {
                    case 0:
                        a = 0;
                        break;
                    case 1:
                        daoConcesionari.crearTaula();
                        Thread.sleep(5000);

                        break;
                    case 2:
                        daoConcesionari.borrarTaula();
                        Thread.sleep(5000);

                        break;
                    case 3:
                        System.out.println("Dades del concesionari a inserir:");
                        System.out.println("dni del concesionari");
                        String dni_conces = sc.next();
                        System.out.println("Nom del concesionari");
                        String nom = sc.next();
                        System.out.println("Llistat de motos a inserir, escogeix cuantes motos desitges inserir max10");
                        int max = sc.nextInt();
                        List<Motos> motos = new ArrayList<>();
                        for (int i = 0; i < max; i++) {
                            System.out.println("Matricula de la moto");
                            String matricula = sc.next();
                            System.out.println("Model moto");
                            String nom_moto = sc.next();
                            System.out.println("Data de matriculacio(YYYY-MM-DD)");
                            String data_matri = sc.next();
                            Motos motos1 = new Motos(nom_moto,matricula,data_matri);

                            motos.add(motos1);
                            System.out.println("Moto num :" + i+1);
                        }

                        Concesionari concesionari = new Concesionari(dni_conces, nom, motos, 10);
                        daoConcesionari.inserir(concesionari);
                        Thread.sleep(5000);

                        break;
                    case 4:
                        System.out.println("Dni del concesionari a cercar");
                        dni_conces = sc.next();
                        daoConcesionari.recuperarPerId(dni_conces);
                        Thread.sleep(5000);

                        break;
                    case 5:
                        daoConcesionari.recuperarTots();
                        Thread.sleep(5000);

                        break;
                    case 6:
                        System.out.println("Dades del concesionari a modificar:");
                        System.out.println("Dni del concesionari");
                        dni_conces = sc.next();
                        System.out.println("Nom del concesionari");
                        nom = sc.next();

                        Concesionari concesionari1 = new Concesionari(dni_conces, nom, null, 10);

                        System.out.println("Nou dni");
                        String dni = sc.next();
                        System.out.println("Nou nom");
                        nom = sc.next();

                        String[] params = new String[]{dni, nom,"10"};

                        daoConcesionari.update(concesionari1, params);
                        Thread.sleep(5000);

                        break;
                    case 7:
                        System.out.println("Dades del concesionari a borrar:");
                        System.out.println("Dni del concesionari");
                        dni = sc.next();
                        daoConcesionari.delete(dni);
                        Thread.sleep(5000);

                        break;
                    case 8:
                        daoConcesionari.getAll();
                        Thread.sleep(5000);

                        break;
                    default:
                        System.out.println("opcio no valida tornara al menu1");
                        a = 0;

                }
                if (a == 0) {
                    menu1();
                }
                {
                    menu3();
                }
                daoConcesionari.closeConnection();
            }
        } catch (Exception e) {
            a = 0;
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }


    public static void main(String[] args) throws ExceptionConnect {
        try {

            menu1();
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }


    }
}