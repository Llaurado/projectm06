import java.net.ConnectException;
import java.sql.SQLException;
import java.util.*;
import java.util.Optional;

public interface DAO<T> {

    Optional<T> get(long id);

    List<T> getAll() throws ExceptionConnect;


    void inserir(T t) throws SQLException;

    void recuperarPerId(String id) throws   ExceptionConnect;

    void getConnection(String url,String user,String pass) throws ExceptionConnect;
    void closeConnection() throws ExceptionConnect;

    void recuperarTots() throws   ExceptionConnect;

    void update(T t, String[] params) throws ExceptionConnect, ExceptionRepeat;

    void delete(String id) throws   ExceptionConnect;

    void crearTaula() throws ExceptionConnect, ExceptionRepeat;

    void borrarTaula() throws   ExceptionConnect;
    void moureMoto(T t,String matricula) throws  ExceptionConnect;
}