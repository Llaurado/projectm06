import java.util.List;

public class Concesionari {

    private String dni_concesionari;
    private  String nom_concesionari;
    private List<Motos> magatzem_motos;
    private int cantmaxmotos;

    public Concesionari(String dni_concesionari, String nom_concesionari, List<Motos> magatzem_motos, int cantmaxmotos) {
        this.dni_concesionari = dni_concesionari;
        this.nom_concesionari = nom_concesionari;
        this.magatzem_motos = magatzem_motos;
        this.cantmaxmotos = cantmaxmotos;
    }

    public String getDni_concesionari() {
        return dni_concesionari;
    }

    public void setDni_concesionari(String dni_concesionari) {
        this.dni_concesionari = dni_concesionari;
    }

    public String getNom_concesionari() {
        return nom_concesionari;
    }

    public void setNom_concesionari(String nom_concesionari) {
        this.nom_concesionari = nom_concesionari;
    }

    public List<Motos> getMagatzem_motos() {
        return magatzem_motos;
    }

    public void setMagatzem_motos(List<Motos> magatzem_motos) {
        this.magatzem_motos = magatzem_motos;
    }

    public int getCantmaxmotos() {
        return cantmaxmotos;
    }

    public void setCantmaxmotos(int cantmaxmotos) {
        this.cantmaxmotos = cantmaxmotos;
    }
}
