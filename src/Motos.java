import java.util.Date;

public class Motos {

    private String nom;
    private String matricula;
    private String data_matriculacio;

    public Motos(String nom, String matricula, String data_matriculacio) {
        this.nom = nom;
        this.matricula = matricula;
        this.data_matriculacio = data_matriculacio;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getData_matriculacio() {
        return data_matriculacio;
    }

    public void setData_matriculacio(String data_matriculacio) {
        this.data_matriculacio = data_matriculacio;
    }
}
