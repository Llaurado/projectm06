
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.sql.SQLException;
import java.util.List;

public class DAOJDBCMotos extends DAOMotos {
    private List<Motos> motos;
    private static final Logger logger = LogManager.getRootLogger();

    private static Connection myconn;

    static {
        try {
            myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/basededatos", "root", "jupiter");
        } catch (Exception e) {

        }
    }

    @Override
    public void getConnection(String url, String user, String pass) throws ExceptionConnect {
        ConexioJDBC conexioJDBC = new ConexioJDBC();
        myconn = conexioJDBC.connec(url, user, pass);

    }

    @Override
    public void crearTaula() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "motos", null);
            Statement statement = myconn.createStatement();


            if (!tables.next()) {
                String query = " create table motos(nom varchar(15),matricula varchar(7) primary key,data_matriculacio date);";
                statement.execute(query);
                System.out.println("Taula creada");
            }
            System.out.println("La taula ja existeix pots comencçar a treballar amb ella");
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }

    @Override
    public void borrarTaula() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "motos", null);
            Statement statement = myconn.createStatement();

            if (tables.next()) {
                String query = " drop table motos";
                statement.execute(query);
                System.out.println("Taula eliminada");
            }
            System.out.println("La taula no existeix");
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }


    @Override
    public List<Motos> getAll() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            Statement mystmn = myconn.createStatement();
            String query = "select * from motos";
            ResultSet rs = mystmn.executeQuery(query);
            Motos motos1 = null;
            while (rs.next()) {

                motos1.setMatricula(rs.getString(2));
                motos1.setNom(rs.getString(1));
                motos1.setData_matriculacio(rs.getString(3));
                motos.add(motos1);
            }
            return motos;
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }


    @Override
    public void inserir(Motos motos) throws SQLException {
        try {

            String sentenciaSQL = "insert into motos (nom,matricula,data_matriculacio)  values (?,?,?)";

            PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);
            sentenciaPreparada.setString(1, motos.getNom());
            sentenciaPreparada.setString(2, motos.getMatricula());
            sentenciaPreparada.setDate(3, Date.valueOf(motos.getData_matriculacio()));
            sentenciaPreparada.executeUpdate();

            System.out.println("Client inserit correctament.");


        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void recuperarPerId(String id) throws ExceptionConnect {

        try {
            String sentenciaSQL = "select * from motos where matricula=" + id;

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            while (rs.next()) {
                System.out.println("Nom" + "\t" + "Matr" + "\t" + "Data_matri");
                System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getDate(3));
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void recuperarTots() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            String sentenciaSQL = "select * from motos";

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            System.out.println("Nom" + "\t" + "Matr" + "\t" + "Data_matri");

            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getDate(3));
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void update(Motos motos, String[] params) throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }


            PreparedStatement preparedStatement=myconn.prepareStatement("update motos set nom=?,matricula=?,data_matriculacio=? where matricula=?");
            preparedStatement.setString(1,params[0]);
            preparedStatement.setString(2,params[1]);
            preparedStatement.setDate(3, Date.valueOf(params[2]));
            preparedStatement.setString(4,motos.getMatricula());
            preparedStatement.executeUpdate();
            System.out.println("update fet");

        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void delete(String id) throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }


            PreparedStatement mystmn = myconn.prepareStatement("delete from motos where matricula=?");
            mystmn.setString(1, id);
            mystmn.executeUpdate();

            System.out.println("s'ha eliminat la moto");
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void closeConnection() {
        ConexioJDBC conexioJDBC = new ConexioJDBC();
        myconn = conexioJDBC.close();


    }
}
