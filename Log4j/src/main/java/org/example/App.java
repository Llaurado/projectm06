package org.example;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Hello world!
 *
 */
public class App 
{
    private static Logger logger = LogManager.getRootLogger();
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        logger.error("logger is configured correctly");
    }
}
